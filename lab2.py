import cv2
import numpy as np

xMouse = 0
yMouse = 0
def Gaussian(image, radius, x, y, sigma, outer=False):
    if sigma == 0:
        sigma = 1
    mask = np.zeros(image.shape, dtype='uint8')
    bluredImg = cv2.GaussianBlur(image, (0, 0), sigma)
    cv2.circle(mask, (x, y), radius, (255, 255, 255), -1)
    mask = cv2.GaussianBlur(mask, (51, 51), 0)
    if outer:
        mask = cv2.bitwise_not(mask)
    result = bluredImg * (mask/255) + image * (1 - mask/255)
    return result.astype(np.ubyte)

def mouseCallBack(event, x, y, flags, param):
    if event == cv2.EVENT_MOUSEMOVE:
        global xMouse, yMouse
        xMouse = x
        yMouse = y

if __name__ == '__main__':
    originalImg = cv2.imread("lw2_test\\cat.png")
    rows, cols = originalImg.shape[:2]
    radius = min(rows, cols) // 2
    cv2.namedWindow("Result")
    cv2.createTrackbar("Radius", "Result", radius, min(cols, rows), lambda x: None)
    cv2.createTrackbar("Sigma", "Result", 1, 100, lambda x: None)
    cv2.setMouseCallback("Result", mouseCallBack)
    while True:
        radius = cv2.getTrackbarPos("Radius", "Result")
        sigma = cv2.getTrackbarPos("Sigma", "Result")
        res = Gaussian(originalImg, radius, xMouse, yMouse, sigma/10, outer=True)
        cv2.imshow("Result", res)
        k = cv2.waitKey(30)
        if k == 27:
            break
    cv2.destroyAllWindows()

