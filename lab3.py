import cv2
from skimage.metrics import mean_squared_error as MSE
from skimage.metrics import peak_signal_noise_ratio as PSNR
from skimage.metrics import structural_similarity as SSIM
import matplotlib.pyplot as plt


if __name__ == '__main__':
    for i in range(1, 4):
        firstImage = cv2.cvtColor(cv2.imread("lw3_test\\test"+str(i)+"_1.jpg"), cv2.COLOR_BGR2RGB)
        secondImage = cv2.cvtColor(cv2.imread("lw3_test\\test"+str(i)+"_2.jpg"), cv2.COLOR_BGR2RGB)
        thirdImage = cv2.cvtColor(cv2.imread("lw3_test\\test"+str(i)+"_3.jpg"), cv2.COLOR_BGR2RGB)
        fourthImage = cv2.blur(firstImage, (10, 10))
        images = [firstImage, secondImage, thirdImage, fourthImage]

        for j in range(4):
            plt.subplot(2, 2, j + 1)
            plt.imshow(images[j])

            if j > 0:
                plt.title("MSE = " + str(round(MSE(images[0], images[j]), 2))
                          + "\nPSNR = " + str(round(PSNR(images[0], images[j]), 2))
                          + "\nSSIM = " + str(round(SSIM(images[0], images[j], multichannel=True), 2)))
        plt.show()

